package md.velobaza.entity;

import lombok.Data;

@Data
public class Error {

    private String message;
    private Integer code;

    public Error(String message, Integer code) {
        this.message = message;
        this.code = code;
    }
}
