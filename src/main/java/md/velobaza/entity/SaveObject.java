package md.velobaza.entity;

import lombok.Data;

@Data
public class SaveObject {
    private Integer objectId;
    private Integer objectTypeId;
    private Integer objectCompanyId;
    private Integer personId;
    private Double  lat;
    private Double  lng;
    private Integer objectStatusId;
}
