package md.velobaza.entity;

import lombok.Data;

@Data
public class ReqObjectByDiapason {

    private Double latStart;
    private Double latEnd;
    private Double lngStart;
    private Double lngEnd;

}
