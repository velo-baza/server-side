package md.velobaza.entity;

import lombok.Data;
import md.velobaza.constants.FileObjectExt;

@Data
public class SaveFileOfObjects {

    private String base64;
    private FileObjectExt fileObjectExt;
    private String fileObjectContentType;
    private Integer personId;
    private Integer orgId;

}
