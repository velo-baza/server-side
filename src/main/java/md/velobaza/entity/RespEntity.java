package md.velobaza.entity;

import lombok.Data;

import java.util.List;

@Data
public class RespEntity {

    private Object data;
    private Error error;

    public RespEntity(Error error) {
        this.error = error;
    }

    public RespEntity(List data) {
        this.data = data;
    }

    public RespEntity(Object data) {
        this.data = data;
    }

}
