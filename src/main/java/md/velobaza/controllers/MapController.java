package md.velobaza.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import md.velobaza.entity.ReqObjectByDiapason;
import md.velobaza.entity.RespEntity;
import md.velobaza.entity.SaveFileOfObjects;
import md.velobaza.entity.SaveObject;
import md.velobaza.services.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("map/")
@ApiOperation(value = "Communication with map entity.", authorizations = {@Authorization(value = "token")})
public class MapController extends BaseController {

    private final MapService mapService;

    private final String OBJECT = "object";
    private final String OBJECTS = OBJECT + "s";

    @Autowired
    public MapController(MapService mapService) {
        this.mapService = mapService;
    }

    @GetMapping(OBJECT + "/categories")
    @ResponseBody
    public ResponseEntity<RespEntity> getAllObjectCategory() {
        return new ResponseEntity<>(new RespEntity(mapService.getObjectCategories()), HttpStatus.OK);
    }

    @GetMapping(OBJECT + "/types")
    @ResponseBody
    public ResponseEntity<RespEntity> getAllObjectTypes(@RequestParam(name = "category_id") Integer categoryId) {
        return new ResponseEntity<>(new RespEntity(mapService.getObjectTypes(categoryId)), HttpStatus.OK);
    }

    @GetMapping(OBJECT + "/companies")
    @ResponseBody
    public ResponseEntity<RespEntity> getAllCompaniesByObjectTypeId(@RequestParam(name = "object_type_id") Integer objectTypeId) {
        return new ResponseEntity<>(new RespEntity(mapService.findCompaniesByObjectTypeId(objectTypeId)), HttpStatus.OK);
    }

    @GetMapping(OBJECT + "/diapason")
    @ResponseBody
    public ResponseEntity<RespEntity> getObjectInformationByDiapason(@RequestBody ReqObjectByDiapason reqObjectByDiapason) {
        return new ResponseEntity<>(new RespEntity(mapService.findObjectsInfByDiapason(reqObjectByDiapason)), HttpStatus.OK);
    }

    @PostMapping(OBJECTS)
    @ResponseBody
    public ResponseEntity<RespEntity> saveObject(@RequestBody SaveObject saveObject) {
        return new ResponseEntity<>( new RespEntity(mapService.saveObject(saveObject))
                                    ,saveObject.getObjectId() == null ? HttpStatus.CREATED : HttpStatus.OK);
    }

    @PostMapping(OBJECTS + "/file")
    @ResponseBody
    @ApiIgnore
    public ResponseEntity<RespEntity> saveFileOfObjects(@RequestBody SaveFileOfObjects saveFileOfObjects) {
        return new ResponseEntity<>(new RespEntity(mapService.saveFileOfObjects(saveFileOfObjects)), HttpStatus.OK);
    }

    @GetMapping(OBJECTS)
    @ResponseBody
    public ResponseEntity<RespEntity> getObjectsByIds(@RequestParam(name = "iDs") String iDs) {
        return new ResponseEntity<>(new RespEntity(mapService.findObjectsByIds(iDs)), HttpStatus.OK);
    }

}
