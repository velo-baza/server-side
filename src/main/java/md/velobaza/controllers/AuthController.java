package md.velobaza.controllers;

import md.velobaza.entity.Error;
import md.velobaza.entity.RespEntity;
import md.velobaza.models.EmailVerifyModel;
import md.velobaza.models.PersonModel;
import md.velobaza.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("auth/")
public class AuthController {

    private final AuthService authService;
    private final JavaMailSender javaMailSender;

    private final String CODE = "code";

    @Autowired
    public AuthController(AuthService authService, JavaMailSender javaMailSender) {
        this.authService = authService;
        this.javaMailSender = javaMailSender;
    }

    @PostMapping(CODE + "/generate")
    @ResponseBody
    public ResponseEntity<RespEntity> generateCode(
            @RequestParam(value = "e_mail") String e_mail
    ) {
        List<Object> objectList = new ArrayList<>();
        EmailVerifyModel emailVerifyModel = (EmailVerifyModel) authService.generateCode(e_mail).getResultList().get(0);
        if (!StringUtils.isEmpty(emailVerifyModel.getEMail())) {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(emailVerifyModel.getEMail());
            msg.setFrom("VelohHarta.md");
            msg.setSubject("Code for log-in to VeloHarta"); // todo add locale
            msg.setText(emailVerifyModel.getCode().toString());
            javaMailSender.send(msg);
        }
        objectList.add(emailVerifyModel);
        RespEntity result = new RespEntity(objectList);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(CODE + "/check")
    public ResponseEntity<RespEntity> checkCode(
            @RequestParam(value = "e_mail", required = false) String e_mail
           ,@RequestParam(value = "code") Integer code
    ){
        Object obj = authService.checkCode(e_mail, code);
        List<Object> objectList = new ArrayList<>();
        RespEntity respEntity;
        if(obj instanceof PersonModel) {
            objectList.add(obj);
            respEntity = new RespEntity(objectList);
            return new ResponseEntity<>(respEntity, HttpStatus.OK);
        } else if (obj instanceof Error) {
            objectList.add(obj);
            respEntity = new RespEntity((Error) obj);
            return new ResponseEntity<>(respEntity, HttpStatus.BAD_REQUEST);
        } else {
            return null;
        }
    }

}