package md.velobaza.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FileObjectExt {

    CSV(10, "csv"),
    XLS(20, "xls"),
    XLSX(30, "xlsx");

    private final int code;
    private final String name;
}
