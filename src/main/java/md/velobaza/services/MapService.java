package md.velobaza.services;

import md.velobaza.config.PathConfigProperty;
import md.velobaza.constants.FileObjectExt;
import md.velobaza.entity.ReqObjectByDiapason;
import md.velobaza.entity.SaveFileOfObjects;
import md.velobaza.entity.SaveObject;
import md.velobaza.models.Object.ObjectDetailsModel;
import md.velobaza.models.Object.ObjectTypeModel;
import md.velobaza.utils.StringUtil;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.List;

import static md.velobaza.models.Object.ObjectCategoryModel.PROC_MAP_FIND_ALL_OBJECT_CATEGORY;
import static md.velobaza.models.Object.ObjectCompanyModel.PROC_MAP_FIND_ALL_COMPANIES_BY_OBJECT_TYPE_ID;
import static md.velobaza.models.Object.ObjectDetailsModel.*;
import static md.velobaza.models.Object.ObjectDiapasonResult.*;
import static md.velobaza.models.Object.ObjectTypeModel.PROC_MAP_FIND_ALL_OBJECT_TYPES_BY_CATEGORY_ID;

@Service
public class MapService extends BaseService{

    public List getObjectCategories() {
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_FIND_ALL_OBJECT_CATEGORY)
                                                    .getResultList();
    }

    public List getObjectTypes(Integer categoryId) {
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_FIND_ALL_OBJECT_TYPES_BY_CATEGORY_ID)
                .setParameter(ObjectTypeModel.P_CATEGORY_ID, categoryId)
                    .getResultList();
    }

    public List findCompaniesByObjectTypeId(Integer objectTypeId) {
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_FIND_ALL_COMPANIES_BY_OBJECT_TYPE_ID)
                .setParameter(P_OBJECT_TYPE_ID, objectTypeId)
                    .getResultList();
    }

    public ObjectDetailsModel saveObject(SaveObject saveObject) {
        return (ObjectDetailsModel) entityManager.createNamedStoredProcedureQuery(PROC_MAP_SAVE_OBJECT)
                                                    .setParameter(P_OBJECT_ID,        saveObject.getObjectId())
                                                    .setParameter(P_OBJECT_TYPE_ID,   saveObject.getObjectTypeId())
                                                    .setParameter(P_OBJECT_COPANY_ID, saveObject.getObjectCompanyId())
                                                    .setParameter(P_PERSON_ID,        saveObject.getPersonId())
                                                    .setParameter(P_LAT,              saveObject.getLat())
                                                    .setParameter(P_LNG,              saveObject.getLng())
                                                    .setParameter(P_OBJECT_STATUS_ID, saveObject.getObjectStatusId())
                                                        .getResultList().get(0);
    }

    public File saveFile() {
        return null;
    }

    public List saveFileOfObjects(SaveFileOfObjects saveFileOfObjects) {
        if (saveFileOfObjects.getFileObjectExt().equals(FileObjectExt.XLS)
                || saveFileOfObjects.getFileObjectExt().equals(FileObjectExt.XLSX)) {
            throw new Error("Not supported type of file");
        }
        String folderFilePath = PathConfigProperty.getInstance().getPersonFileObjectPathByPersonId(saveFileOfObjects.getPersonId());
        folderFilePath = folderFilePath +  File.pathSeparatorChar
                         + Instant.now().toString() + "_" + saveFileOfObjects.getFileObjectContentType()
                         + "." + saveFileOfObjects.getFileObjectExt().getName();
        File file = new File(folderFilePath);
        // todo convert to csv if file is xls or xlsx
        try {
            Files.write(file.toPath(), StringUtil.Base64toBytes(saveFileOfObjects.getBase64()));
        } catch (IOException e) {
            throw new Error("", e);
        }
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_SAVE_FILE_OF_OBJECTS)
                                .setParameter(P_PATH_OF_FILE, file.getAbsolutePath())
                                .getResultList();
    }

    public List findObjectsInfByDiapason(ReqObjectByDiapason reqObjByDiapason) {
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_FIND_OBJECT_INF_BY_DIAPASON)
                .setParameter(P_LAT_START, reqObjByDiapason.getLatStart())
                .setParameter(P_LAT_END,   reqObjByDiapason.getLatEnd())
                .setParameter(P_LNG_START, reqObjByDiapason.getLngStart())
                .setParameter(P_LNG_END,   reqObjByDiapason.getLngEnd())
                    .getResultList();
    }

    public List findObjectsByIds(String iDs) {
        return entityManager.createNamedStoredProcedureQuery(PROC_MAP_FIND_OBJECTS_BY_IDS)
                .setParameter(P_IDS, iDs)
                    .getResultList();
    }

}
