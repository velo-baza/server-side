package md.velobaza.services;

import md.velobaza.entity.Error;
import org.springframework.stereotype.Service;

import javax.persistence.Query;

import java.sql.SQLException;

import static md.velobaza.models.EmailVerifyModel.*;

@Service
public class AuthService extends BaseService {

    public Query generateCode(String e_mail) {
        return entityManager.createNamedStoredProcedureQuery(PROC_AUTH_GENERATE_CODE_BY_EMAIL)
                .setParameter(P_IN_E_MAIL, e_mail);
    }

    public Object checkCode(String e_mail, Integer code) {
        Query query = entityManager.createNamedStoredProcedureQuery(PROC_AUTH_CHECK_CODE)
                .setParameter(P_IN_E_MAIL, e_mail)
                .setParameter(P_IN_CODE, code);
        try {
            return query.getResultList().get(0);
        } catch (Exception e) {
            SQLException sqlException = ((SQLException) e.getCause().getCause());
            return new Error(sqlException.getMessage(), new Integer(sqlException.getSQLState()));
        }
    }

}
