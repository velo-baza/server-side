package md.velobaza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeloBazaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeloBazaApplication.class, args);
	}

}
