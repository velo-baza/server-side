package md.velobaza.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class PersonModel extends BaseModel {

    @Column(name = "grade_id")
    Integer gradeId;

    @Column(name = "grade_code")
    String gradeCode;

    @Column(name = "e_mail")
    String eMail;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "full_name")
    String fullName;

    @Column(name = "nick_name")
    String nickName;

    @Column(name = "lang_id")
    Integer lang_id;

    @Column(name = "token")
    String token;
}
