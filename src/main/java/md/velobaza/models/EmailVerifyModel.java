package md.velobaza.models;

import lombok.Data;

import javax.persistence.*;

import java.util.Date;

import static md.velobaza.models.EmailVerifyModel.*;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery( name = PROC_AUTH_GENERATE_CODE_BY_EMAIL
                                   ,procedureName = "auth_generate_code_by_email"
                                   ,parameters = @StoredProcedureParameter(name = P_IN_E_MAIL, type = String.class)
                                   ,resultClasses = EmailVerifyModel.class
                                  )
       ,@NamedStoredProcedureQuery( name = PROC_AUTH_CHECK_CODE
                                   ,procedureName = "auth_check_code"
                                   ,resultClasses = PersonModel.class
                                   ,parameters = { @StoredProcedureParameter(name = P_IN_E_MAIL, type = String.class)
                                                  ,@StoredProcedureParameter(name = P_IN_CODE, type = Integer.class)
                                                 }
                                  )
})
@Data
@Entity
public class EmailVerifyModel {

    public final static String PROC_AUTH_GENERATE_CODE_BY_EMAIL = "authGenerateCodeByEmail";
    public final static String PROC_AUTH_CHECK_CODE = "authCheckCode";

    public final static String P_IN_E_MAIL = "p_email";
    public final static String P_IN_CODE = "p_code";

    @Id
    @Column(name = "code")
    private Integer code;

    @Column(name = "e_mail")
    private String eMail;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "expiration_date")
    private Date expirationDate;

    @Column(name = "person_id")
    private int personId;
}
