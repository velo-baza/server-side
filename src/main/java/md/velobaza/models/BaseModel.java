package md.velobaza.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class BaseModel {
    @Id
    @Column(name = "id")
    private Integer id;
}
