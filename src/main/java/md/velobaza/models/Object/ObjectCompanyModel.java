package md.velobaza.models.Object;

import lombok.Data;
import md.velobaza.models.BaseModel;

import javax.persistence.*;

import static md.velobaza.models.Object.ObjectCompanyModel.PROC_MAP_FIND_ALL_COMPANIES_BY_OBJECT_TYPE_ID;
import static md.velobaza.models.Object.ObjectDetailsModel.*;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery( name = PROC_MAP_FIND_ALL_COMPANIES_BY_OBJECT_TYPE_ID
                                   ,procedureName = "map_find_all_companies_by_object_type_id"
                                   ,parameters = @StoredProcedureParameter(name = P_OBJECT_TYPE_ID, type = Integer.class)
                                   ,resultClasses = ObjectCompanyModel.class
        )
})
@Data
@Entity
public class ObjectCompanyModel extends BaseModel {

        final static public String PROC_MAP_FIND_ALL_COMPANIES_BY_OBJECT_TYPE_ID = "mapFindAllCompaniesByObjectTypeId";

        @Column(name = "object_company_abbreviation")
        String objectCompanyAbbreviation;

        @Column(name = "object_company_full_name")
        String objectCompanyFullName;
}
