package md.velobaza.models.Object;

import lombok.Data;
import md.velobaza.models.BaseModel;

import javax.persistence.*;

import static md.velobaza.models.Object.ObjectCategoryModel.PROC_MAP_FIND_ALL_OBJECT_CATEGORY;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery( name = PROC_MAP_FIND_ALL_OBJECT_CATEGORY
                                   ,procedureName = "map_find_all_object_category"
                                   ,resultClasses = ObjectCategoryModel.class
        )
})
@Data
@Entity
public class ObjectCategoryModel extends BaseModel {

    public final static String PROC_MAP_FIND_ALL_OBJECT_CATEGORY = "mapFindAllObjectCategory";

    @Column(name = "code")
    String code;
}
