package md.velobaza.models.Object;

import lombok.Data;
import md.velobaza.models.BaseDateModel;

import javax.persistence.*;

import static md.velobaza.models.Object.ObjectDetailsModel.*;

@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery( name = PROC_MAP_SAVE_OBJECT
                               ,procedureName = "map_save_object"
                               ,parameters = { @StoredProcedureParameter(name = P_OBJECT_ID,        type = Integer.class)
                                              ,@StoredProcedureParameter(name = P_OBJECT_TYPE_ID,   type = Integer.class)
                                              ,@StoredProcedureParameter(name = P_OBJECT_STATUS_ID, type = Integer.class)
                                              ,@StoredProcedureParameter(name = P_PERSON_ID,        type = Integer.class)
                                              ,@StoredProcedureParameter(name = P_LAT,              type = Double.class)
                                              ,@StoredProcedureParameter(name = P_LNG,              type = Double.class)
                                              ,@StoredProcedureParameter(name = P_OBJECT_STATUS_ID, type = Integer.class)
                                             }
                               ,resultClasses = ObjectDetailsModel.class
                              )
   ,@NamedStoredProcedureQuery( name = PROC_MAP_FIND_OBJECTS_BY_IDS
                               ,procedureName = "map_find_objects_by_ids"
                               ,parameters = @StoredProcedureParameter(name = P_IDS, type = String.class)
                               ,resultClasses = ObjectDetailsModel.class
                              )
})
@Data
@Entity
public class ObjectDetailsModel extends BaseDateModel {

    public static final String PROC_MAP_SAVE_OBJECT = "mapSaveObject";
    public static final String PROC_MAP_SAVE_FILE_OF_OBJECTS = "mapSaveFileOfObjects";
    public static final String PROC_MAP_FIND_OBJECTS_BY_IDS = "mapFindObjectsByIds";

    public static final String P_OBJECT_ID = "p_object_id";
    public static final String P_OBJECT_TYPE_ID = "p_object_type_id";
    public static final String P_OBJECT_COPANY_ID = "p_object_company_id";
    public static final String P_PERSON_ID = "p_person_id";
    public static final String P_LAT = "p_lat";
    public static final String P_LNG = "p_lng";
    public static final String P_OBJECT_STATUS_ID = "p_object_status_id";
    public static final String P_IDS = "p_ids";

    public static final String P_PATH_OF_FILE = "p_path_of_temp_file";

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "object_type_id")
    private Integer objectTypeId;

    @Column(name = "object_type_code")
    private String objectTypeCode;

    @Column(name = "object_category_id")
    private Integer objectCategoryId;

    @Column(name = "object_category_code")
    private String objectCategoryCode;

    @Column(name = "object_status_id")
    private Integer objectStatusId;

    @Column(name = "object_status_code")
    private String objectStatusCode;

    @Column(name = "person_id")
    private Integer personId;

    @Column(name = "object_company_id")
    private Integer objectCompanyId;

    @Column(name = "object_company_abbreviation")
    private String objectCompanyAbbreviation;

}
