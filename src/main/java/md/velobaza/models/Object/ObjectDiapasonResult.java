package md.velobaza.models.Object;

import md.velobaza.models.BaseDateModel;

import javax.persistence.Entity;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.StoredProcedureParameter;

import static md.velobaza.models.Object.ObjectDiapasonResult.*;

@NamedStoredProcedureQuery( name = PROC_MAP_FIND_OBJECT_INF_BY_DIAPASON
                           ,procedureName = "map_find_objects_by_diapason"
                           ,parameters = { @StoredProcedureParameter(name = P_LAT_START, type = Double.class)
                                          ,@StoredProcedureParameter(name = P_LAT_END,   type = Double.class)
                                          ,@StoredProcedureParameter(name = P_LNG_START, type = Double.class)
                                          ,@StoredProcedureParameter(name = P_LNG_END,   type = Double.class)
                                         }
                           ,resultClasses = ObjectDiapasonResult.class
                           )
@Entity
public class ObjectDiapasonResult extends BaseDateModel {
    
    public static final String PROC_MAP_FIND_OBJECT_INF_BY_DIAPASON = "mapFindObjectsInfByDiapason";

    public static final String P_LAT_START = "p_lat_start";
    public static final String P_LAT_END   = "p_lat_end";
    public static final String P_LNG_START = "p_lng_start";
    public static final String P_LNG_END   = "p_lng_end";
    
}
