package md.velobaza.models.Object;

import lombok.Data;
import md.velobaza.models.BaseModel;

import javax.persistence.*;

import static md.velobaza.models.Object.ObjectTypeModel.PROC_MAP_FIND_ALL_OBJECT_TYPES_BY_CATEGORY_ID;
import static md.velobaza.models.Object.ObjectTypeModel.P_CATEGORY_ID;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery( name = PROC_MAP_FIND_ALL_OBJECT_TYPES_BY_CATEGORY_ID
                ,procedureName = "map_find_all_object_types_by_category_id"
                ,parameters = @StoredProcedureParameter( name = P_CATEGORY_ID
                                                        ,mode = ParameterMode.IN
                                                        ,type = Integer.class)
                ,resultClasses = ObjectTypeModel.class
        )
})
@Data
@Entity
public class ObjectTypeModel extends BaseModel {

    public final static String PROC_MAP_FIND_ALL_OBJECT_TYPES_BY_CATEGORY_ID = "mapFindAllObjectTypesByCategoryId";

    public final static String P_CATEGORY_ID = "p_category_id";

    @Column(name = "code")
    String code;

    @Column(name = "object_category_id")
    Integer categoryId;
}
