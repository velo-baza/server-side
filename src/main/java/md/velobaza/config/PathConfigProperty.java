package md.velobaza.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;

@ConfigurationProperties(prefix = "folder")
@Data
public class PathConfigProperty {

   public String home;
   public String person;
   public String organization;
   public String file;
   public String object;

   public static PathConfigProperty getInstance() {
       return new PathConfigProperty();
   }

   public String getPersonPathByPersonIdId(Integer personId) {
       return getHome() + File.pathSeparatorChar + getPerson() + File.pathSeparatorChar + personId;
   }

   public String getPersonFilePathByPersonId(Integer personId) {
       return getPersonPathByPersonIdId(personId) + File.pathSeparatorChar + getFile();
   }

   public String getPersonFileObjectPathByPersonId(Integer personId) {
       return getPersonFilePathByPersonId(personId) + File.pathSeparatorChar + getObject();
   }

   public String getOrgFilePathByOrgId(Integer personId, Integer orgId) {
       return getPersonPathByPersonIdId(personId) + File.pathSeparatorChar + getOrganization()
                                          + File.pathSeparatorChar + orgId
                                          + File.pathSeparatorChar + getFile();
   }

}
