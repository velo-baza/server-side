package md.velobaza.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket api() {
        List<AuthorizationScope> authorizationScopes = Arrays.asList( new AuthorizationScope("write", "")
                                                                     ,new AuthorizationScope("read", ""));
        List<GrantType> grantTypes = Arrays.asList(new ResourceOwnerPasswordCredentialsGrant(""));
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(ApiInfo.DEFAULT).securitySchemes(Collections.singletonList(new OAuth( "oauth2"
                                                                                              ,authorizationScopes
                                                                                              ,grantTypes)))
                .select()
                .apis(RequestHandlerSelectors.basePackage("md.veloharta.controllers"))
                .paths(PathSelectors.regex("/.*"))
                .build();
    }

}
