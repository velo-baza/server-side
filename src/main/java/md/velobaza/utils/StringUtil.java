package md.velobaza.utils;

import java.util.Base64;

public class StringUtil {

    private StringUtil() {}

    public static byte[] Base64toBytes(String base64) {
        return Base64.getDecoder().decode(base64);
    }

}
