drop table if exists organization;

create table if not exists organization (
   id           int          not null auto_increment
  ,name         varchar(150) not null
  ,e_mail       varchar(254)     null
  ,phone_number varchar(16)      null
  ,person_id    int          not null
  ,constraint pk_organization primary key (id)
  ,constraint fk_organization_person_id foreign key (person_id) references person(id)
  ,constraint u_name   unique (name)
  ,constraint u_e_mail unique (e_mail)
);

# example
select *
  from organization;
