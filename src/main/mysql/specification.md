# create TABLE
```mysql
drop table if exists table_name;

create table if not exists table_name (
   id int not null auto_increment
   -- another field
  ,primary key (id)

);

# example
select *
  from table_name;

```
# create VIEW
```mysql
drop view if exists view_name;

create view view_name

as
  select
       tn.*
      from m_table_name as tn;
```
# create PROCEDURE
```mysql
drop procedure if exists create_user;

create procedure create_user (
   in p_id           int
  ,out p_errors_list varchar(250)
)
proc_label:begin
  -- body
end
```
# create FUNCTION
<!-- todo need will add specification by add new function in database after create first function-->