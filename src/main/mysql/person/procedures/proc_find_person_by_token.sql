drop procedure if exists person_find_by_token;

create procedure person_find_by_token (
  in p_token varchar(50)
)
proc_label:begin
  select *
    from person_view
    where token = p_token;
end;

call person_find_by_token((select token from person limit 1));