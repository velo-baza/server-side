drop procedure if exists person_save;

create procedure person_save (
   in p_person_id  int
  ,in p_first_name varchar(100)
  ,in p_last_name  varchar(100)
  ,in p_nick_name  varchar(30)
  ,in p_lang_id    smallint
)
proc_label:begin
    declare
        v_old_nick_name varchar(30);

    if (exists(select *
               from person as p
               where p.id != p_person_id
                 and p.nick_name = p_nick_name)) then
        signal sqlstate '32442' set message_text = 'nickname already exist on another person';
    end if;

    select
        p.nick_name into v_old_nick_name
    from person as p
    where p.id = p_person_id;

    update person set
        first_name = p_first_name
       ,last_name  = p_last_name
       ,nick_name  = p_nick_name
       ,lang_id    = p_lang_id
    where id = p_person_id;

    if (p_nick_name <> v_old_nick_name) then
        insert into person_nickname_history (
              old_nickname
             ,new_nickname
             ,person_id
        ) values (
             v_old_nick_name
            ,p_nick_name
            ,p_person_id);
    end if;

    select *
      from person_view as p
      where p.person_id = p_person_id;

end;

# example
call person_save(11, 'Eugen', 'Țurcan', 'etur', 2);
select * from person_view where person_id = 12;
call person_save(12,'Tatiana','Pascal', 'fwefe', 2);
select * from person_nickname_history;