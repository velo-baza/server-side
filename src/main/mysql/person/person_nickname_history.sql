drop table if exists person_nickname_history;

create table if not exists person_nickname_history (
   id           int         not null auto_increment
  ,old_nickname varchar(30)     null
  ,new_nickname varchar(30) not null
  ,date_changed timestamp   default current_timestamp
  ,person_id    int
  ,constraint fk_person foreign key (person_id) references person(id)
  ,constraint pk_person_nickname_history primary key (id)
);

select *
    from person_nickname_history;
