drop table if exists person;

create table if not exists person (
    id         int             not null auto_increment
   ,first_name varchar(100)        null
   ,last_name  varchar(100)        null
   ,nick_name  varchar(30)         null
   ,e_mail     varchar(254)    not null
   ,grade_id   smallint            null
   ,token      varchar(50)         null
   ,lang_id    smallint            null
   ,constraint pk_person       primary key (id)
   ,constraint fk_person_grade foreign key (grade_id) references grade(id)
   ,constraint u_nick_name unique (nick_name)
   ,constraint u_e_mail    unique (e_mail)
);

select * from person;

insert into person (
    last_name, first_name, e_mail, grade_id
)
values
    ('Lutenco', 'Andrei', 'andreilutenco@gmail.com', 1),
    ('Lapurji', 'Edgar', 'lapurjiedgar@gmai.com', 1),
    ('Bulgac', 'Octavian', 'octav.bulgac@gmail.com', 1),
    ('Drahnea', 'Vlad', 'vlad.drahnea@gmail.com', 1),
    ('Cicala', 'Igor', 'igorcicala7@gmail.com', 1),
    ('Bujoreanu', 'Victor', 'bujoreanu.victor@gmail.com', 1),
    ('Mihai', 'Moldoveanu', 'mihaimold@gmail.com', 1),
    ('Bulat', 'Alexandru', 'alex85978@gmail.com', 1),
    ('Plăcintă', 'Alexandru', 'alexplacinta946@gmail.com', 1),
    ('Popa', 'Ana Maria', 'anamariapopa9@gmail.com', 1),
    ('Țurcan', 'Eugen', 'țurcankatea@gmail.com', 1),
    ('Pascal', 'Tatiana', 'pascal.tatyana@gmail.com', 1),
    ('Natalia', 'Slepuhin', 'n.slepuhin@gmail.com', 1),
    ('Iecmen', 'Andrey', 'zayafetwords@gmail.com', 1),
    ('Us', 'Vladimir', 'us.vladimir@gmail.com', 1),
    ('Giussani Roguljic', 'Natalia', 'naati.guissani@live.com.ar', 1);

update person as p set p.token = conv(floor(rand() * 99999999999999), 20, 36); # todo temporary auth
