drop view if exists person_view;

create view person_view
as
    select
         p.id                                   as id
        ,g.id                                   as grade_id
        ,g.code                                 as grade_code
        ,p.e_mail                               as e_mail
        ,p.first_name                           as first_name
        ,p.last_name                            as last_name
        ,concat(p.first_name, ' ', p.last_name) as full_name
        ,p.nick_name                            as nick_name
        ,p.lang_id                              as lang_id
        ,p.token                                as token
      from person as p
      left join grade as g on g.id = p.grade_id;

# test
select * from person_view;