drop table if exists grade;

create table if not exists grade (
    id   smallint    not null auto_increment
   ,code varchar(50) not null
   ,constraint pk_grade primary key (id)
   ,constraint u_grade_code unique (code)
);

insert into grade (code)
values ('admin');
insert into grade (code)
values ('volunteer');

select * from grade;

# todo create function for get id for every grade