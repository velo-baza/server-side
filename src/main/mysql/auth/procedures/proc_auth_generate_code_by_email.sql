drop procedure if exists auth_generate_code_by_email;

create procedure auth_generate_code_by_email(
  in p_email varchar(254)
)
proc_label:begin
    declare v_created_code int default null;
    declare v_person_id    int default null;

    select
          p.id into v_person_id
        from person as p
        where p.e_mail = p_email;

    if (v_person_id is null) then
        # todo need to find "how we would signal errors codes"
        signal sqlstate '24242' SET message_text = 'Hey, person with each e-mail does not exist';
    else
        set v_created_code = (select round(rand() * 10000000));
        insert into email_verify(
           code
          ,person_id
        ) value (
           v_created_code
          ,v_person_id
        );
        select *
          from email_verify_view as evv
          where evv.code = v_created_code;
    end if;
end;

# example
call auth_generate_code_by_email((select
                                       concat(p.e_mail, '')
                                    from person as p limit 1));
