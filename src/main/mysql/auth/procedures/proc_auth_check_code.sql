drop procedure if exists auth_check_code;

create procedure auth_check_code(
   in p_email varchar(254)
  ,in p_code  int
)
proc_label:begin
    if (exists(select *
                 from email_verify as ev
                 inner join person p on ev.person_id = p.id
                 where ev.code  = p_code
                   and p.e_mail = p_email)) then
        delete from email_verify as ev
               where code = p_code;
        select *
          from person_view as pv
          where e_mail = p_email;
    else
        signal sqlstate '35423' set message_text = 'Hey, not found user.';
    end if;
end;

# test
call auth_check_code('alex85978@gmail.com', 4538517);
