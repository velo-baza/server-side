drop view if exists email_verify_view;

create view email_verify_view
    as
        select
             ev.code
            ,ev.created_date
            ,ev.expiration_date
            ,ev.person_id
            ,p.e_mail
          from email_verify as ev
          inner join person as p on p.id = ev.person_id;

select *
  from email_verify_view;

