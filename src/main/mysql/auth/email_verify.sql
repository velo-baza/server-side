drop table if exists email_verify;

create table if not exists email_verify (
     code            int             not null
    ,person_id       int             not null
    ,created_date    timestamp       not null default current_timestamp
    ,expiration_date datetime        not null default (current_timestamp + interval 2 minute)
    ,constraint fk_email_verify_person foreign key (person_id) references person(id)
    ,constraint pk_email_verify        primary key (code)
);

select * from email_verify;
