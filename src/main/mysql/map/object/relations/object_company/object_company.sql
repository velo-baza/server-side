drop table if exists object_company;

create table if not exists object_company (
   id           int          not null auto_increment
  ,abbreviation varchar(30)  not null
  ,full_name    varchar(150) not null
  ,primary key (id)
  ,constraint u_object_company_full_name    unique(full_name)
  ,constraint u_object_company_abbreviation unique(abbreviation)
);

# example
select *
  from object_company;
