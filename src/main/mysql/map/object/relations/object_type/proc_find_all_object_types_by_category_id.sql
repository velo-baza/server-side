drop procedure if exists map_find_all_object_types_by_category_id;

create procedure map_find_all_object_types_by_category_id(
  in p_category_id int
)
begin
    select *
      from object_type as ot
      where ot.object_category_id = p_category_id;
end;

# example
call map_find_all_object_types_by_category_id(1);
