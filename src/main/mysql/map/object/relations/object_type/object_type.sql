drop table if exists object_type;

create table if not exists object_type (
     id                 int              not null auto_increment
    ,code               varchar(50)      not null
    ,details            varchar(250)         null
    ,object_category_id int              not null
    ,constraint pk_object_type                 primary key (id)
    ,constraint pk_object_category_object_type foreign key (object_category_id) references object_type (id)
    ,constraint u_code_category_id unique (code, object_category_id)
);

insert into object_type
    (code, object_category_id)
values
    ('bicycle_parking', 1)
   ,('bicycle_repair_station', 1);

select * from object_type;
#  todo create functions for get code for id, idk
