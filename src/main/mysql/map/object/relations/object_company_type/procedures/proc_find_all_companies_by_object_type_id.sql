drop procedure if exists map_find_all_companies_by_object_type_id;

create procedure map_find_all_companies_by_object_type_id (
    in p_object_type_id int
)
begin
    select
         oc.id
        ,oc.abbreviation        as object_company_abbreviation
        ,oc.full_name           as object_company_full_name
      from object_company_type  as oct
      inner join object_company as oc on oc.id = oct.object_company_id
      inner join object_type    as ot on ot.id = oct.object_type_id
                                     and ot.id = p_object_type_id ;
end;

# example
call map_find_all_companies_by_object_type_id(1);
