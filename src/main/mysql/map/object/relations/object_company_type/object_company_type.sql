drop table if exists object_company_type;

create table if not exists object_company_type (
   id                int not null auto_increment
  ,object_company_id int not null
  ,object_type_id    int not null
  ,primary key (id)
  ,constraint fk_comp_type_object_company foreign key(object_company_id) references object_company(id)
  ,constraint fk_comp_type_object_type    foreign key(object_type_id)    references object_type(id)
  ,constraint u_object_company_type unique(object_company_id, object_type_id)
);

# example
select *
from object_company_type;
