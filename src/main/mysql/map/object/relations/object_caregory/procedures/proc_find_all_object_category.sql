drop procedure if exists map_find_all_object_category;

create procedure map_find_all_object_category()
begin
    select *
      from object_category;
end;

# example
call map_find_all_object_category()