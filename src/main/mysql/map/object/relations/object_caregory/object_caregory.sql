drop table if exists object_category;

create table if not exists object_category (
     id    smallint         not null auto_increment
    ,code  varchar(50)      not null
    ,constraint pk_object_category primary key (id)
    ,constraint u_object_category_code unique (code)
);

insert into object_category (code)
values ('transport');

select * from object_category;

# todo create function for get id or code, idk