drop table if exists object_status;

create table if not exists object_status (
   id   int         not null auto_increment
  ,code varchar(30) not null
  ,name varchar(50) not null
  ,constraint pk_object_status_id primary key (id)
  ,constraint fk_object_status_code unique (code)
  ,constraint fk_object_status_name unique (name)
);

insert into object_status
    (code, name)
values
    ('visible', 'Visible')
   ,('invisible', 'Invisible')
   ,('is_deleted', 'Is deleted');

# example
select *
  from object_status;
