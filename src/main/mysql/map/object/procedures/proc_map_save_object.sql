drop procedure if exists map_save_object;

create procedure map_save_object(
    in p_object_id         int
   ,in p_object_type_id    int
   ,in p_object_company_id int
   ,in p_person_id         int
   ,in p_lat               decimal(10, 8)
   ,in p_lng               decimal(11, 8)
   ,in p_object_status_id  int
)
proc_label:begin
    declare
        v_exist_obj_id int default null;

    select
        o.id into v_exist_obj_id
    from object as o
    where (    o.lat = p_lat
           and o.lng = p_lng)
      and o.object_type_id    = p_object_type_id
      and o.object_company_id = p_object_company_id
    limit 1;

    if(    v_exist_obj_id is not null
       and v_exist_obj_id != p_object_id) then
        signal sqlstate '44421' set message_text = 'The same object already created.';
    end if;

    if (exists(select *
               from object as o
               where id = p_object_id)) then
        update object set
            lat               = p_lat
           ,lng               = p_lng
           ,object_type_id    = p_object_type_id
           ,object_company_id = p_object_company_id
           ,person_id         = p_person_id
           ,object_status_id  = p_object_status_id
           ,updated_date      = current_timestamp()
        where id = p_object_id;
        select *
        from object_view as ov
        where ov.id = p_object_id;
    else
        insert into object (
            object_type_id, person_id, lat, lng, object_status_id, object_company_id
        ) values (
            p_object_type_id, p_person_id, p_lat, p_lat, p_object_status_id, p_object_company_id
        );
        select *
        from object_view as ov
        where ov.id = last_insert_id();
    end if;

end;

# example
call map_save_object();
