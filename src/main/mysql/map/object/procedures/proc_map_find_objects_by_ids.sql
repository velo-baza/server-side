drop procedure if exists map_find_objects_by_ids;

create procedure map_find_objects_by_ids(
    in p_ids varchar(2000)
)
begin
    select
        *
      from object_view as ov
      where find_in_set(ov.id, p_ids) <> 0;
end;

# example
call map_find_objects_by_ids('2,1,4,5,6,');
