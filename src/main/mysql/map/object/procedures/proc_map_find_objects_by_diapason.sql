drop procedure if exists map_find_objects_by_diapason;

create procedure map_find_objects_by_diapason(
   in p_lat_start decimal(10, 8)
  ,in p_lat_end   decimal(10, 8)
  ,in p_lng_start decimal(11, 8)
  ,in p_lng_end   decimal(11, 8)
)
begin
    select
         o.id
        ,o.created_date
        ,o.updated_date
      from object as o
      where o.lng <= p_lng_start
        and o.lng >= p_lng_end
        and (    o.lat >= p_lat_start
             and o.lat <= p_lat_end);
end;

# example
call map_find_objects_by_diapason(53.46662300, 55, -1, -3);
select * from object_view;
