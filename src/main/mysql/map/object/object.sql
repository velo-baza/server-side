drop table if exists object;

create table if not exists object (
     id                int            not null auto_increment
    ,object_type_id    int            not null
    ,person_id         int            not null
    ,lat               decimal(10, 8)     null
    ,lng               decimal(11, 8)     null
    ,street_id         int                null # todo add table with streets
    ,number_of_building varchar(10)       null
    ,object_status_id  int            not null
    ,created_date      timestamp      not null default current_timestamp
    ,updated_date      timestamp          null
    ,object_company_id int                null
    ,constraint pk_object primary key (id)
    ,constraint fk_object_person        foreign key (person_id)         references person(id)
    ,constraint fk_object_object_type   foreign key (object_type_id)    references object_type(id)
    ,constraint fk_object_status_status foreign key (object_status_id)  references object_status(id)
    ,constraint fk_object_company_id    foreign key (object_company_id) references object_company(id)
);

insert into object (object_type_id, person_id, lat, lng, object_status_id)
values (1, 1, 53.4666230, -2.2350430, 1);

# example
select * from object
