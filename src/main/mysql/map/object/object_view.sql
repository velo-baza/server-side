drop view if exists object_view;

create view object_view
as
    select
         o.id                    as id
        ,o.person_id             as person_id
        ,o.lat                   as latitude
        ,o.lng                   as longitude
        ,o.created_date          as created_date
        ,o.updated_date          as updated_date
        ,ot.id                   as object_type_id
        ,ot.code                 as object_type_code
        ,oc.id                   as object_category_id
        ,oc.code                 as object_category_code
        ,os.id                   as object_status_id
        ,os.code                 as object_status_code
        ,ocy.id                  as object_company_id
        ,ocy.abbreviation        as object_company_abbreviation
      from object                as o
      inner join object_type     as ot  on o.object_type_id = ot.id
      inner join object_category as oc  on oc.id = ot.object_category_id
      inner join object_status   as os  on os.id = o.object_status_id
      left join  object_company  as ocy on ocy.id = o.object_company_id;

# example
select *
  from object_view;

select * from
